Reading on `DataTransfer` object. `DataTransferListItem`. These are from events
of the HTML5 drag and drop APIs. Also read on the `paste` event.

Saw how React Dropzone uses hooks to tie in methods that handle file upload.

- https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
- https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer also `.getData`
- https://react-dropzone.js.org/ and use of `FileReader()`/`readAsBinaryString`

Dropping multiple the images onto a queue is useful for multiple levels of a
floorplan, which is my usecase.

Need a progress area/view. Settings sidebar for tuning tilesize and zoom level.
Output area to show zoom levels for testing. Queue area. Option to apply
settings to all queue items or only some. Errors for uploads that aren't files.
File sizes (human readable) and dimensions. Need to be able to drop through the
zoom levels and drag around using translate3d (also `will-change`)

Only support 50% zoom reductions, that's the standard and otherwise any map
library that tries to render it will break; also it's called Dicehalf.

Let them choose the start zoom and then how many levels deep. For instance,
instead of 100%, which might be way too up close and zoomed in for them, allow
80%. Then it'll do 40%, 20%, 10% etc.

During reductions there's bound to be overshoot which needs to be filled with a
background colour. Also, support a top and left offset value from 0 to the tile
size. That's base-level overshoot.

Draw a basic grid, probably via canvas? To show the tile outline. This is a must
and would have saved me hours if I had it.

Need to implement tabs for the sidebar. One for file uploading and the other for
settings/configuration of the layer.
