# Dicehalf

_WIP. Focusing on ImageData() resizing, PNG encoding, and zip archives. There's
no frontend yet. Progress notes are written in here below the introduction_

Static client-side application for pyramidally tiling large images for use in
maps like Leaflet. https://en.wikipedia.org/wiki/Tiled_web_map.

It's repeated crop and resize. The image is diced into 256px<sup>2</sup> tiles,
then resized to half its size. Repeat this until you're happy with the minimum
zoom level. After processing, you download a zip of the tile directory tree.

If the image doesn't fit nicely in the tiles, it can be extended with a
background colour. Cropping or extending the original might help this.

---

Canvas can do both cropping and resizing easily. There are some hacks for
getting nice looking resizes though, either with `ctx.imageSmoothingEnabled` or
by partially resizing with one canvas, moving image data into another canvas,
then doing the rest.

This should be able to run in Node or a Web Worker, and neither have canvas
support natively. Node has a package called `canvas` in C++ which uses Cairo to
simulate one. Web Workers _sort of_ have [Offscreen Canvas][1] but that's only
ready in Chrome; Firefox only has the WebGL context and the feature behind a
flag which hasn't moved since version 46.

Running in a worker is good because some of my basemaps produce 4000+ images
when tiled, and that's a lot of time.

Speaking of performance, there's also https://github.com/gpujs/gpu.js which
re-implements a subset of JS in JS to run on GPUs... I'm not going to use it
because it's huge, even gzipped.

Workers can pass around `ImageData()` which is a `Uint8ClampedArray` (1D) of 0
to 255. Little bit of math can translate 2D to 1D indices, and people have built
programs to work with those arrays.

## Resizing with interpolation

Here are some image resizers in pure JS that use BiLinear (or BiCubic)
interpolation and operate strictly on `ImageData`:

- https://github.com/LinusU/resize-image-data/ which is beautiful code and only
  around 40 lines for resizing + bilinear. Lots of function overhead, however.

- http://strauss.pas.nu/js-bilinear-interpolation.html mentions concerns of
  overhead but has nicely explained code for both bilinear and bicubic.

- https://github.com/taisel/JS-Image-Resizer is 430 lines of most copy pasted
  methods for handling both RGB and RGBA images. The author was trying to be as
  performant as possible - think "unrolling loops" - but it makes for very hard
  to read code. Has a worker implementation. The 19kb file gzips down to 1.5kb.

I'll pick `resize-image-data` which is favouring code readability over
performance. It's short and very easy to understand.

That covers resizing and interpolation, but not dicing.

## Cropping

To crop a section from a `Uint8ClampedArray`, you can use for loops for reading
values off one array and into another. This works out to O(n<sup>3</sup>). It
might be able to be improved by [`.slice()`][2], but the ECMAScript [spec][3]
doesn't look promising for that either.

Several for-loops and careful array indexing is all cropping needs. Remember
that boundary checks will be needed to optionally fill in a background colour
for tiles that extend the image.

## PNG

All of these operations have been performed on `Uint8ClampedArray`s to avoid
canvas. Unfortunately, saving that a zip file isn't a PNG - it's a binary of raw
pixel data, so **a 5000x5000 transparent image is 100MB**. This needs to be
converted to a PNG to be useful. As of f43c3f, I've piped the data into a canvas
and then used `.toBlob` to export the canvas as a PNG. This is fast and free
since the browser handles it.

To avoid canvas though, I need to look at a few PNG implementations in pure JS.
They exist, and are very similar to JSZip since they each deflate using pako,
which is zlib, and use crc32 functions.

A fully featured PNG encoder/decoder can be around 900 LoC. However, I've seen
basic PNG encoders done in about 200 lines. The first encoder I found worked
entirely in strings since it was made before TypedArray existed. I put it as a
snippet of this repository.

I'll use parts of the encoder from this project:
https://github.com/image-js/fast-png/blob/master/src/PNGEncoder.js

It needs to have the `iobuffer` dependency removed since that's 500kb and
focused on Node, not browsers. Otherwise the implementation is nice and uses
Uint8Array.

Deflation is nice since some tiles could be mostly or entirely transparent, but
I wonder if most users would just run zopfli, or their compression algorithm of
choice, on the directory tree once they download. ~~It might not be worth it to
implement that since it's not saving bandwidth - the images are created and
downloaded client-side, afterall.~~ JSZip includes pako (zlib) so it's free.

## Zip

Need to be able to download all the files together. I found two implementations
of zip archives in JS. [JSZip][4] is 95kb minified and is very actively
developed (14 days ago). The other isn't really worth mentioning since it's 2-7
years old, needed to request 5-7 other scripts at runtime, and doesn't provide a
dist bundle or size.

[1]: https://developer.mozilla.org/en-US/docs/Web/API/OffscreenCanvas
[2]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/slice
[3]: https://tc39.github.io/ecma262/#sec-array.prototype.slice
[4]: https://github.com/Stuk/jszip

---

## Dev TODO

Currently using Parcel since I'm very very tired of Webpack configs. However,
Parcel tries to be magic and does a lot under the hood. Too much. Pulls in +8000
packages to run. Would like to switch to pikapkg for ES modules if possible.
