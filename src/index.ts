import * as path from 'path';
import * as fs from 'fs';

import { decode, encode } from 'fast-png';
import resize from 'resize-image-data';
import { crop } from './operations/crop';
import { Image } from './types';

const rel = (name: string) => {
  console.log(name);
  return path.join(__dirname, '..', name);
};
const originalPNG = fs.readFileSync(rel('image.png'));

const decoded = decode(originalPNG);
const { width: srcWidth, height: srcHeight, channels } = decoded;

if (decoded.data instanceof Uint16Array) {
  throw new Error('PNG was decoded as Uint16Array. Need a Uint8');
}
// Now Typescript is happy with this as a Uint8
const { data } = decoded;

const srcImage: Image = {
  data: Uint8ClampedArray.from(data),
  height: srcHeight,
  width: srcWidth,
  channels,
};

// TODO: Ask via `read()` and `commander.js`
const TILE_SIZE = 256;

for (let x = -50; x < srcWidth; x += TILE_SIZE) {
  for (let y = -50; y < srcHeight; y += TILE_SIZE) {
    saveCrop(x, y, TILE_SIZE, TILE_SIZE);
  }
}

function saveCrop(x: number, y: number, w: number, h: number) {
  const cropped = crop(srcImage, x, y, w, h);
  const newPNG = encode({
    ...cropped,
    // Type issue
    data: cropped.data as unknown as Uint8Array,
  });
  fs.writeFileSync(rel(`tiles/crop-x${x}-y${y}-w${w}-h${h}.png`), newPNG);
}

function saveResize(w: number, h: number) {
  const resized = resize(srcImage, w, h);
  const newPNG = encode({
    ...resized,
    channels: srcImage.channels,
    // Type issue
    data: resized.data as unknown as Uint8Array,
  });
  fs.writeFileSync(rel(`tiles/resize-w${w}-h${h}.png`), newPNG);
}
