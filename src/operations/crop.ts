import { Image } from '../types';

function crop(
  srcImage: Image,
  x0: number, y0: number,
  w: number, h: number,
  // This will never fail. It will be clamped to 0 or 255
  offsetFill: number[] = [0, 0, 0, 0]
) {
  const { width: srcWidth, height: srcHeight, data, channels } = srcImage;

  const cropData = new Uint8ClampedArray(w * h * channels);
  // TODO: PERF: Add modulo src{Height,Width} to avoid looping huge offsets
  for (let cy = y0; cy < (h + y0); cy++) {
    for (let cx = x0; cx < (w + x0); cx++) {

      let srcPos = ((cy * srcWidth) + cx) * channels;
      // Undo the offset else the crop will be offset too
      let pos = (((cy - y0) * w) + (cx - x0)) * channels;

      for (let i = 0; i < channels; i++) {
        cropData[pos++] = (cx < 0 || cx > srcWidth || cy < 0 || cy > srcHeight)
          ? offsetFill[i] || 0
          : data[srcPos++];
      }
    }
  }
  const cropImage: Image = {
    data: cropData,
    width: w,
    height: h,
    channels,
  };
  return cropImage;
}

export { crop };
